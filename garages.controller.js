// garage.controller.js
const express = require('express');
const router = express.Router();

// connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

// import middleware
const { validateGarageData } = require('./validator.middleware');

// Création d'une route GET pour afficher la base de données express_garage, garage
router.get('/', (req, res) => {
    // Sélectionner tous les garages
    connection.query('SELECT * FROM garage', (err, results) => {
      // Gestion des erreurs
      if (err) {
        res.status(500).send('Erreur lors de la récupération des garage');
        console.log(err);
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // GET : Rechercher un garage par son nom
  router.get('/name/:name', (req, res) => {
    const name = req.params.name;
    // Requête préparée plus sécurisée
    connection.execute('SELECT * FROM garage WHERE name = ?', [name], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la récupération des garages');
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // GET : Rechercher une garage par son ID
  router.get('/id/:id', (req, res) => {
    const id = req.params.id;
    // Requête préparée plus sécurisée
    connection.execute('SELECT * FROM garage WHERE garage_id = ?', [id], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la récupération du garage');
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // POST : Ajouter un nouveau garage dans la base de données
  router.post('/add-new-garage', validateGarageData, (req, res) => {
    const { name, email } = req.body;
    // Insérer une nouvelle garage
    connection.execute('INSERT INTO garage (name, email) VALUES (?, ?)', [name, email], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de l\'ajout du garage');
      } else {
        // Confirmation de l'addition avec ID
        res.status(201).send(`garage ajouté avec l'ID ${results.insertId}`);
      }
    });
  });
  // PUT : Met à jour une garage par son ID
  router.put('/:id', validateGarageData, (req, res) => {
    const { name, email } = req.body;
    const id = req.params.id;
    // Mettre à jour la garage
    connection.execute('UPDATE garage SET name = ?, email = ? WHERE garage_id = ?', [id, name, email], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la mise à jour du garage');
      } else {
        // Confirmation de la requête
        res.send(`garage mis à jour.`);
      }
    });
  });
  // DELETE : supprimer une garage avec l'ID
  router.delete('/:id', (req, res) => {
    const id = req.params.id;
    // Supprimer la garage
    connection.execute('DELETE FROM garage WHERE garage_id = ?', [id], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la suppression du garage');
      } else {
        // Confirmation de la requête
        res.send(`garage supprimé.`);
      }
    });
  });

module.exports = router;
