const validateGarageData = (req, res, next) => {
    const { name, email } = req.body;

    if (!name || !email ) {
        return res.status(400).json({ message : 'le nom du garage et le mail sont obligatoires.' });
    }

    // mail regex
    const mail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    // la méthode test permet de savoir si la chaîne de caractère correspon à la chaîne regex
    if (!mail.test(email)) {
        return res.status(400).json({ message : 'adresse email invalide.'})
    }
    next();
};

module.exports = { validateGarageData };