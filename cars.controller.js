// cars.controller.js
const express = require('express');
const router = express.Router();

// connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db");

// Création d'une route GET pour afficher la base de données express_garage
router.get('/', (req, res) => {
    // Sélectionner toutes les voitures
    connection.query('SELECT * FROM car', (err, results) => {
      // Gestion des erreurs
      if (err) {
        res.status(500).send('Erreur lors de la récupération des voitures');
        console.log(err);
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // GET : Rechercher une voiture par sa marque 
  router.get('/brand/:brand', (req, res) => {
    const brand = req.params.brand;
    // Requête préparée plus sécurisée
    connection.execute('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la récupération des voitures');
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // GET : Rechercher une voiture par son ID
  router.get('/id/:id', (req, res) => {
    const id = req.params.id;
    // Requête préparée plus sécurisée
    connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la récupération de la voiture');
      } else {
        // Renvoie du JSON
        res.json(results);
      }
    });
  });
  // POST : Ajouter une nouvelle voiture dans la base de données
  router.post('/add-new', (req, res) => {
    const { brand, model } = req.body;
    // Insérer une nouvelle voiture
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de l\'ajout de la voiture');
      } else {
        // Confirmation de l'addition avec ID
        res.status(201).send(`Voiture ajouté avec l'ID ${results.insertId}`);
      }
    });
  });
  // PUT : Met à jour une voiture par son ID
  router.put('/:id', (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Mettre à jour la voiture
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [id, brand, model], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la mise à jour de la voiture');
      } else {
        // Confirmation de la requête
        res.send(`Voiture mise à jour.`);
      }
    });
  });
  // DELETE : supprimer une voiture avec l'ID
  router.delete('/:id', (req, res) => {
    const id = req.params.id;
    // Supprimer la voiture
    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
      if (err) {
        res.status(500).send('Erreur lors de la suppression de la voiture');
      } else {
        // Confirmation de la requête
        res.send(`Voiture supprimée.`);
      }
    });
  });

module.exports = router;
