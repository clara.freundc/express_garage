require('dotenv').config();
const express = require('express');
const cors = require('cors');
// import le module de connexion (l'élément déplacé)
const connection = require('./conf/db');
const app = express();

// app.quelquechose = requête, la parse en JSON.
app.use(express.json());

// CORS
app.use(cors({
  origin: 'http://localhost:8080'
}));

// Import et urilisation du Middleware logger
// A placer AVANT les controllers
const { logRequest } = require('./logger.middleware.js');
app.use(logRequest);

const carsController = require('./cars.controller.js');
const garagesController = require('./garages.controller.js');

app.use('/cars', carsController);
app.use('/garages', garagesController);

// Parse en url
app.use(
  express.urlencoded({
    extended: true,
  })
);

const port = 3000;

// Route racine
app.get('/', (req, res) => {
  res.send("Hello World!");
})

// Démarre le serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});





