const mysql = require('mysql2');

// Pool de connexions à la BDD express_garage
const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD
  });
  
  // Connexion
  connection.getConnection((err) => {
    if (err instanceof Error) {
      console.log('getConnection error:', err);
      return;
    }
  });



  module.exports  = connection;